---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---

This course is an introductory-level course in the Python programming language, aimed
at all staff at the British Geological Survey who may want to use programming in their work,
or gain an understanding of the Python programming language and what it can do. 
It assumes no prior knowledge of Python or computer programming (coding). The course is based
on the Software Carpentry Institute's [Introduction to Python](https://swcarpentry.github.io/python-novice-gapminder/) 
training course, adapted for use at BGS. 

> ## Prerequisites
>
> 1. No prior knowledge of Python or programming is required. 
>
> 2. Learners should install a suite of tools called "Anaconda" before the class starts. 
>    Anaconda is a collection of apps that allow you to write and run Python. Anaconda 
>    can be found in the BGS company portal which is in the start menu on Windows machines.  
>
> 3. Learners must get the "gapminder" data before class starts:
>    please download and unzip the file
>    [python-novice-gapminder-data.zip]({{page.root}}/files/python-novice-gapminder-data.zip).
>    [Gapminder](https://www.gapminder.org/) is a collection of global socio-economic data, 
>    useful for demonstrating how Python can read and analyse data.
>
>    Please see [the setup instructions][lesson-setup]
>    for details.
{: .prereq}

{% include links.md %}
