---
layout: page
title: "Setup"
permalink: /setup/
root: ..
---

## Installing Python Using Anaconda

[Python][python] is a popular language for scientific computing, and great for
general-purpose programming as well. Installing all of its scientific packages
individually can be a bit difficult, however, so we recommend the all-in-one
installer [Anaconda][anaconda].

At BGS, you can find Anaconda in the Company Portal. The Company Portal is 
found by searching for it in the start menu, and then searching for 
"BGS Anaconda". Click on the install button on the app, and wait for the
installation to complete. Once complete, you should be able to find
"Anaconda Navigator" in the start menu. 

Don't worry if you run into issues however, we can spend the first bit of the course
troubleshooting setup problems.


### Linux

At BGS, you should first set up a Linux environment using WSL 
(Windows Subsystem for Linux).

http://handbooks.glpages.ad.nerc.ac.uk/informatics-handbook/#/content/resources/capabilities/wsl

Then, you can install conda, or Anaconda, using the same 
steps as found on the [Anaconda website](www.anaconda.org)

## Getting the Data

The data we will be using is taken from the [gapminder][gapminder] dataset.
To obtain it, download and unzip the file
[python-novice-gapminder-data.zip]({{page.root}}/files/python-novice-gapminder-data.zip).

We will be using the **Spyder** program to write, edit, and run Python programs. Spyder is 
part of a large suite of Python software supplied with the **Anaconda** Python distribution,
a software suite aimed at scientific computing and data science users.


[anaconda]: https://www.anaconda.com/
[anaconda-mac]: https://www.anaconda.com/download/#macos
[anaconda-linux]: https://www.anaconda.com/download/#linux
[anaconda-windows]: https://www.anaconda.com/download/#windows
[gapminder]: https://en.wikipedia.org/wiki/Gapminder_Foundation
[python]: https://python.org
[video-mac]: https://www.youtube.com/watch?v=TcSAln46u9U
[video-windows]: https://www.youtube.com/watch?v=xxQ0mzZ8UvA
